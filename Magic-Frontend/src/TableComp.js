import React,{useContext} from "react";
import { MagicContext } from './helpers/magicProvider';

import { Table } from 'semantic-ui-react';


export default function TableComp(){
    const {tableData,loginData}=useContext(MagicContext);    
    return(
        <>
        {loginData.isLoggedIn && (
         <Table celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>ID</Table.HeaderCell>
                    <Table.HeaderCell>First Name</Table.HeaderCell>
                    <Table.HeaderCell>Last Name</Table.HeaderCell>
                    <Table.HeaderCell>Email Address</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

                <Table.Body>
                {Object.values(tableData.usrData).map(
                        ({ id, first_name, last_name,email }) => {
                            return (
                            <Table.Row>
                                <Table.Cell key="{id}">{id}</Table.Cell>
                                <Table.Cell>{first_name}</Table.Cell>
                                <Table.Cell>{last_name}</Table.Cell>
                                <Table.Cell>{email}</Table.Cell>
                            </Table.Row>
                            )
                        }
                        )}
                </Table.Body>
            </Table>  
        )}
        </>
    )
}